# Flectra Community / stock-logistics-workflow

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[stock_return_request](stock_return_request/) | 2.0.1.0.2| Stock Return Request
[delivery_package_default_shipping_weight](delivery_package_default_shipping_weight/) | 2.0.1.0.1| Set default package shipping weight according to packaging
[stock_picking_show_return](stock_picking_show_return/) | 2.0.1.0.1| Show returns on stock pickings
[stock_split_picking](stock_split_picking/) | 2.0.1.1.0| Split a picking in two not transferred pickings
[stock_move_change_source_location](stock_move_change_source_location/) | 2.0.1.0.0|         This module allows you to change the source location of a stock move from the        picking    
[stock_partner_delivery_window](stock_partner_delivery_window/) | 2.0.1.2.0| Define preferred delivery time windows for partners
[stock_production_lot_active](stock_production_lot_active/) | 2.0.1.0.0|         Allow to archive/unarchive a lot.
[stock_picking_line_sequence](stock_picking_line_sequence/) | 2.0.1.0.0| Manages the order of stock moves by displaying its sequence
[stock_delivery_note](stock_delivery_note/) | 2.0.1.0.1|         This module allows to fill in a delivery note that will be displayed        on delivery report
[procurement_auto_create_group_carrier](procurement_auto_create_group_carrier/) | 2.0.1.1.0| Procurement Auto Create Group Carrier
[stock_picking_show_backorder](stock_picking_show_backorder/) | 2.0.1.0.0| Provides a new field on stock pickings, allowing to display the corresponding backorders.
[stock_move_assign_picking_hook](stock_move_assign_picking_hook/) | 2.0.1.1.1| Stock Move picking assignment hook
[stock_picking_back2draft](stock_picking_back2draft/) | 2.0.1.0.0| Reopen cancelled pickings
[stock_quant_package_dimension](stock_quant_package_dimension/) | 2.0.2.2.0| Use dimensions on packages
[stock_picking_backorder_strategy](stock_picking_backorder_strategy/) | 2.0.1.0.0| Picking backordering strategies
[stock_lock_lot](stock_lock_lot/) | 2.0.1.0.0| Stock Lock Lot
[stock_valuation_no_developer_mode](stock_valuation_no_developer_mode/) | 2.0.1.0.1| Stock valuation layer no developer mode
[delivery_total_weight_from_packaging](delivery_total_weight_from_packaging/) | 2.0.1.0.0| Include packaging weight on move, transfer and package.
[stock_putaway_by_route](stock_putaway_by_route/) | 2.0.1.0.0| Add a match by route on putaway, after product and categories
[stock_push_delay](stock_push_delay/) | 2.0.1.0.1| Manual evaluation of Push rules
[stock_move_quick_lot](stock_move_quick_lot/) | 2.0.1.0.0| Set lot name and end date directly on picking operations
[stock_owner_restriction](stock_owner_restriction/) | 2.0.1.0.1| Do not reserve quantity with assigned owner
[stock_picking_group_by_partner_by_carrier_by_date](stock_picking_group_by_partner_by_carrier_by_date/) | 2.0.1.0.1| Stock Picking: group by partner and carrier and scheduled date
[stock_picking_send_by_mail](stock_picking_send_by_mail/) | 2.0.1.0.0| Send stock picking by email
[stock_picking_purchase_order_link](stock_picking_purchase_order_link/) | 2.0.1.0.1| Link between picking and purchase order
[stock_picking_group_by_partner_by_carrier](stock_picking_group_by_partner_by_carrier/) | 2.0.1.4.2| Stock Picking: group by partner and carrier
[stock_picking_batch_extended](stock_picking_batch_extended/) | 2.0.1.0.0| Allows manage a lot of pickings in batch
[stock_picking_warn_message](stock_picking_warn_message/) | 2.0.1.0.1|         Add a popup warning on picking to ensure warning is populated
[stock_picking_sale_order_link](stock_picking_sale_order_link/) | 2.0.1.0.1| Link between picking and sale order
[stock_picking_restrict_cancel_with_orig_move](stock_picking_restrict_cancel_with_orig_move/) | 2.0.1.0.1| Restrict cancellation of dest moves according to origin.
[stock_quant_package_dimension_total_weight_from_packaging](stock_quant_package_dimension_total_weight_from_packaging/) | 2.0.1.1.0| Estimated weight of a package
[stock_auto_move](stock_auto_move/) | 2.0.1.0.0| Automatic Move Processing
[stock_picking_filter_lot](stock_picking_filter_lot/) | 2.0.1.0.1| In picking out lots' selection, filter lots based on their location
[sale_stock_mto_as_mts_orderpoint](sale_stock_mto_as_mts_orderpoint/) | 2.0.1.0.1| Materialize need from MTO route through orderpoint
[stock_move_line_auto_fill](stock_move_line_auto_fill/) | 2.0.1.0.0| Stock Move Line auto fill
[stock_picking_invoice_link](stock_picking_invoice_link/) | 2.0.1.1.1| Adds link between pickings and invoices
[stock_quant_package_product_packaging](stock_quant_package_product_packaging/) | 2.0.1.1.0| Use product packagings on packages
[stock_no_negative](stock_no_negative/) | 2.0.1.0.1| Disallow negative stock levels by default
[stock_picking_cancel_reason](stock_picking_cancel_reason/) | 2.0.1.0.0| Stock Picking Cancel Reason
[delivery_procurement_group_carrier](delivery_procurement_group_carrier/) | 2.0.1.1.0| Delivery Procurement Group Carrier
[product_supplierinfo_for_customer_picking](product_supplierinfo_for_customer_picking/) | 2.0.1.0.1| This module makes the product customer code visible in the stock moves of a picking.
[purchase_stock_picking_invoice_link](purchase_stock_picking_invoice_link/) | 2.0.1.0.0| Adds link between purchases, pickings and invoices
[stock_restrict_lot](stock_restrict_lot/) | 2.0.1.1.1| Base module that add back the concept of restrict lot on stock move
[stock_picking_mass_action](stock_picking_mass_action/) | 2.0.1.0.1| Stock Picking Mass Action
[stock_valuation_layer_by_category](stock_valuation_layer_by_category/) | 2.0.1.0.0| Display stock valuation layer by category
[stock_putaway_hook](stock_putaway_hook/) | 2.0.1.0.0| Add hooks allowing modules to add more putaway strategies
[stock_picking_auto_create_lot](stock_picking_auto_create_lot/) | 2.0.1.0.0| Auto create lots for incoming pickings


