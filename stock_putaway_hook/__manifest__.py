# Copyright 2020 Camptocamp
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    "name": "Stock Putaway Hooks",
    "summary": "Add hooks allowing modules to add more putaway strategies",
    "version": "2.0.1.0.0",
    "category": "Hidden",
    "website": "https://gitlab.com/flectra-community/stock-logistics-workflow",
    "author": "Camptocamp, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "application": True,
    "depends": ["stock"],
    "data": [],
}
