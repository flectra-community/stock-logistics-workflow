{
    "name": "Procurement Auto Create Group Carrier",
    "Summary": """Adds the delivery carrier in the procurement group data.""",
    "version": "2.0.1.1.0",
    "development_status": "Alpha",
    "author": "Camptocamp, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/stock-logistics-workflow",
    "category": "Warehouse Managemen",
    "depends": [
        "delivery_procurement_group_carrier",
        "procurement_auto_create_group",
    ],
    "installable": True,
    "auto_install": True,
    "license": "AGPL-3",
}
