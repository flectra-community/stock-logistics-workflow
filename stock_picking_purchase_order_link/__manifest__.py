# Copyright 2019 ForgeFlow S.L.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Stock Picking Purchase Order Link",
    "summary": "Link between picking and purchase order",
    "version": "2.0.1.0.1",
    "license": "AGPL-3",
    "author": "ForgeFlow S.L., Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/stock-logistics-workflow",
    "application": False,
    "installable": True,
    "depends": ["purchase_stock"],
    "data": ["views/stock_picking_view.xml"],
}
