# Copyright 2022 Jarsa
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).
{
    "name": "Stock Valuation Layer no developer mode",
    "summary": "Stock valuation layer no developer mode",
    "version": "2.0.1.0.1",
    "category": "Stock",
    "website": "https://gitlab.com/flectra-community/stock-logistics-workflow",
    "author": "Jarsa, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "depends": ["stock_account"],
    "data": [
        "views/stock_valuation_layer.xml",
    ],
}
