# Copyright 2020 ForgeFlow S.L.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Stock Picking Warn Message",
    "summary": """
        Add a popup warning on picking to ensure warning is populated""",
    "version": "2.0.1.0.1",
    "license": "AGPL-3",
    "author": "ForgeFlow, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/stock-logistics-workflow",
    "depends": ["stock"],
    "data": ["views/stock_picking_views.xml"],
}
