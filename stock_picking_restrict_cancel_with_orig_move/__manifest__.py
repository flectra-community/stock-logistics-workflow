# Copyright 2018 Camptocamp SA
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
{
    "name": "Stock Picking Restrict Cancel with Original Moves",
    "summary": "Restrict cancellation of dest moves according to origin.",
    "version": "2.0.1.0.1",
    "category": "Warehouse",
    "website": "https://gitlab.com/flectra-community/stock-logistics-workflow",
    "author": "Camptocamp, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "depends": ["stock"],
}
