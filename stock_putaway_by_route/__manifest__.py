# Copyright 2020 Camptocamp
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    "name": "Stock Putaway By Route",
    "summary": "Add a match by route on putaway, after product and categories",
    "version": "2.0.1.0.0",
    "category": "Inventory",
    "website": "https://gitlab.com/flectra-community/stock-logistics-workflow",
    "author": "Camptocamp, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "application": False,
    "depends": ["stock_putaway_hook"],
    "data": ["views/stock_putaway_rule_views.xml"],
}
