# Copyright 2020 Camptocamp
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    "name": "Stock Move picking assignment hook",
    "Summary": "Base module that adds an hook to override picking assignment on moves.",
    "version": "2.0.1.1.1",
    "development_status": "Production/Stable",
    "author": "Camptocamp, BCIM, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/stock-logistics-workflow",
    "category": "Warehouse Management",
    "depends": ["stock"],
    "installable": True,
    "license": "AGPL-3",
}
